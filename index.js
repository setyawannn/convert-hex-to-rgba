const express = require('express')
const app = express()
const port = 3000



// function convert hex to rgba
const convertHexToRGBA = (hexCode, opacity = 1) => {
  let hex = hexCode.replace('#', '');

  if (hex.length === 3) {
    hex = `${hex[0]}${hex[0]}${hex[1]}${hex[1]}${hex[2]}${hex[2]}`;
  }

  const r = parseInt(hex.substring(0, 2), 16);
  const g = parseInt(hex.substring(2, 4), 16);
  const b = parseInt(hex.substring(4, 6), 16);

  /* Backward compatibility for whole number based opacity values. */
  if (opacity > 1 && opacity <= 100) {
    opacity = opacity / 100;
  }

  return `rgba(${r},${g},${b},${opacity})`;
};



app.get('/api', (req, res) => {
  let hex = req.params.hex
  let rgba = convertHexToRGBA(hex)

  // response
  let response = {
    color: rgba
  }

  res.json(response)
})


app.listen(port, () => console.log(`Example app listening on port ${port}!`)) 